from datetime import datetime
from io import BytesIO
from typing import BinaryIO

from PIL import Image, ImageDraw, ImageFilter
from fastapi import UploadFile

from src.font import get_regular_font, get_bold_font


def paste_background_with_shade(original_image: Image, image_to_paste: BinaryIO):
    background_image = Image.open(image_to_paste).convert("RGBA").resize((1920, 1080))
    background_image = background_image.filter(ImageFilter.GaussianBlur(radius=1.5))
    original_image.paste(background_image)
    opacity = 100
    shade = Image.new("RGBA", (1920, 1080), color=(0, 0, 0, opacity))
    original_image.paste(shade, mask=shade)


def paste_logo_position_1(original_image: Image, image_to_paste: BinaryIO):
    image_to_paste = Image.open(image_to_paste).convert("RGBA").resize((400, 125))
    original_image.paste(image_to_paste, (120, 50), image_to_paste)


def paste_logo_position_2(original_image: Image, image_to_paste: BinaryIO):
    image_to_paste = Image.open(image_to_paste).convert("RGBA").resize((400, 125))
    original_image.paste(image_to_paste, (760, 50), image_to_paste)


def paste_logo_position_3(original_image: Image, image_to_paste: BinaryIO):
    image_to_paste = Image.open(image_to_paste).convert("RGBA").resize((400, 125))
    original_image.paste(image_to_paste, (1460, 50), image_to_paste)


def draw_rating(image_draw: ImageDraw, rating: str, draw_right: bool = False):
    font_size = 100
    static_y = 720
    text = f"{rating.upper()} CPT"
    if not draw_right:
        return image_draw.text((70, static_y), text, font=get_bold_font(font_size))
    calculated_text_size = int(image_draw.textlength(text, font=get_bold_font(font_size)))
    return image_draw.text((1850 - calculated_text_size, static_y), text, font=get_bold_font(font_size))


def draw_call_sign(image_draw: ImageDraw, call_sign: str, draw_right: bool = False):
    font_size = 72
    static_y = 820
    text = f"{call_sign.upper()}"
    if not draw_right:
        return image_draw.text((70, static_y), text, font=get_bold_font(font_size))
    calculated_text_size = int(image_draw.textlength(text, font=get_bold_font(font_size)))
    return image_draw.text((1850 - calculated_text_size, static_y), text, font=get_bold_font(font_size))


def numerals(day):
    date_suffix = ["th", "st", "nd", "rd"]

    if day % 10 in [1, 2, 3] and day not in [11, 12, 13]:
        return date_suffix[day % 10]
    else:
        return date_suffix[0]


def draw_date_time(image_draw: ImageDraw, start_date_time: datetime, end_date_time: datetime, draw_right: bool = False):
    font_size = 40
    static_y = 900
    text = f"{start_date_time.day}{numerals(start_date_time.day)} {start_date_time.strftime('%B')} | {start_date_time.strftime('%H%M')}-{end_date_time.strftime('%H%M')}Z"
    if not draw_right:
        return image_draw.text((70, static_y), text, font=get_regular_font(font_size))
    calculated_text_size = int(image_draw.textlength(text, font=get_regular_font(font_size)))
    return image_draw.text((1850 - calculated_text_size, static_y), text, font=get_regular_font(font_size))


def draw_candidate(image_draw: ImageDraw, candidate: str, draw_right: bool = False):
    font_size = 40
    static_y = 950
    text = f"Candidate | {candidate}"
    if not draw_right:
        return image_draw.text((70, static_y), text, font=get_regular_font(font_size))
    calculated_text_size = int(image_draw.textlength(text, font=get_regular_font(font_size)))
    return image_draw.text((1850 - calculated_text_size, static_y), text, font=get_regular_font(font_size))


def generate_cpt_banner(background_image: UploadFile, rating: str, call_sign: str, start_date_time: datetime,
                        end_date_time: datetime, candidate: str,
                        draw_right: bool = False, logo_1: UploadFile = None, logo_2: UploadFile = None,
                        logo_3: UploadFile = None) -> BytesIO:
    image = Image.new("RGBA", (1920, 1080))

    paste_background_with_shade(image, background_image.file)

    if logo_1:
        paste_logo_position_1(image, logo_1.file)
    if logo_2:
        paste_logo_position_2(image, logo_2.file)
    if logo_3:
        paste_logo_position_3(image, logo_3.file)

    draw = ImageDraw.Draw(image)

    draw_rating(draw, rating, draw_right)
    draw_call_sign(draw, call_sign, draw_right)
    draw_date_time(draw, start_date_time, end_date_time, draw_right)
    draw_candidate(draw, candidate, draw_right)
    output = BytesIO()
    image = image.convert('RGB')
    image.save(output, "JPEG")
    output.seek(0)

    return output
