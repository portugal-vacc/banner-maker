import os
from datetime import datetime
from io import BytesIO
from typing import List, Optional

import requests
from flask import Blueprint, render_template, request, send_file
from pydantic import BaseModel

from src.cpt import numerals

main_page = Blueprint('main_page', __name__, template_folder='templates')

templates_config_url = "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/config.json"


class Template(BaseModel):
    name: str
    type: str
    call_sign: Optional[str]
    rating: Optional[str]
    text_right: bool = False
    background_image_url: str
    logo_1_url: Optional[str]
    logo_2_url: Optional[str]
    logo_3_url: Optional[str]


class Config(BaseModel):
    templates: List[Template]


def init_config(url: str) -> Config:
    response = requests.get(url)
    response.raise_for_status()
    return Config(**response.json())


@main_page.route("/")
def flask_main():
    templates = init_config(templates_config_url)
    return render_template('banner_maker.html', templates_json=templates.json(), templates_config=templates)


class FormData(BaseModel):
    name: Optional[str]
    start_date_time: datetime
    end_date_time: datetime
    controller_name: Optional[str]
    title: Optional[str]
    subtitle: Optional[str]

    @property
    def selected_template(self) -> Template:
        for template in init_config(templates_config_url).templates:
            if template.name == self.name:
                return template


def parse_form_data(raw_form) -> FormData:
    return FormData(name=raw_form.get('select_template'),
                    start_date_time=request.form.get('start_date_time'),
                    end_date_time=request.form.get('end_date_time'),
                    controller_name=request.form.get('controller_name'),
                    subtitle=request.form.get('subtitle'))


@main_page.route("/form", methods=["POST"])
def receive_form():
    form_data = parse_form_data(request.form)
    template = form_data.selected_template

    background_image = requests.get(template.background_image_url)

    files = {"background_image": ('template.png', BytesIO(background_image.content), 'image/png')}
    if template.logo_1_url:
        logo_1 = requests.get(template.logo_1_url)

        files["logo_1"] = ('logo_1.png', BytesIO(logo_1.content), 'image/png')
    if template.logo_2_url:
        logo_2 = requests.get(template.logo_2_url)

        files["logo_2"] = ('logo_2.png', BytesIO(logo_2.content), 'image/png')
    if template.logo_3_url:
        logo_3 = requests.get(template.logo_3_url)

        files["logo_3"] = ('logo_3.png', BytesIO(logo_3.content), 'image/png')

    file_name = "banner_maker"
    if template.type == "cpt":
        cpt_params = dict(rating=template.rating, call_sign=template.call_sign,
                          start_date_time=form_data.start_date_time, end_date_time=form_data.end_date_time,
                          draw_right=template.text_right, candidate=form_data.controller_name)
        response = requests.post("http://localhost:8000/api/generate/cpt",
                                 files=files,
                                 params=cpt_params,
                                 headers={"api-key": os.getenv("API_KEY", "e3140817-bb26-4ac8-88a7-e5ca78579dd0")})
        file_name = form_data.controller_name
    elif template.type == "event":
        event_params = dict(title=template.name, sub_title=form_data.subtitle,
                            description=f"{form_data.start_date_time.day}{numerals(form_data.start_date_time.day)}"
                                        f" {form_data.start_date_time.strftime('%B')} |"
                                        f" {form_data.start_date_time.strftime('%H%M')}-"
                                        f"{form_data.end_date_time.strftime('%H%M')}Z",
                            draw_right=template.text_right)
        response = requests.post("http://localhost:8000/api/generate/event",
                                 files=files,
                                 params=event_params,
                                 headers={"api-key": os.getenv("API_KEY", "e3140817-bb26-4ac8-88a7-e5ca78579dd0")})
        file_name = template.name

    response.raise_for_status()
    return send_file(BytesIO(response.content), as_attachment=True, mimetype='image/jpeg',
                     download_name=f'{file_name}.jpg')
