from io import BytesIO
from typing import BinaryIO

from PIL import Image, ImageDraw
from fastapi import UploadFile

from src.font import get_bold_font, get_regular_font


def paste_background_with_shade(original_image: Image, image_to_paste: BinaryIO):
    background_image = Image.open(image_to_paste).convert("RGBA").resize((1920, 1080))
    original_image.paste(background_image)
    opacity = 100
    shade = Image.new("RGBA", (1920, 1080), color=(0, 0, 0, opacity))
    original_image.paste(shade, mask=shade)


def paste_logo_position_1(original_image: Image, image_to_paste: BinaryIO):
    image_to_paste = Image.open(image_to_paste).convert("RGBA").resize((400, 125))
    original_image.paste(image_to_paste, (120, 50), image_to_paste)


def paste_logo_position_2(original_image: Image, image_to_paste: BinaryIO):
    image_to_paste = Image.open(image_to_paste).convert("RGBA").resize((400, 125))
    original_image.paste(image_to_paste, (760, 50), image_to_paste)


def paste_logo_position_3(original_image: Image, image_to_paste: BinaryIO):
    image_to_paste = Image.open(image_to_paste).convert("RGBA").resize((400, 125))
    original_image.paste(image_to_paste, (1460, 50), image_to_paste)


def draw_title(image_draw: ImageDraw, text: str, draw_right: bool = False):
    font_size = 80
    static_y = 800
    if not draw_right:
        return image_draw.text((70, static_y), text, font=get_bold_font(font_size))
    calculated_text_size = int(image_draw.textlength(text, font=get_bold_font(font_size)))
    return image_draw.text((1850 - calculated_text_size, static_y), text, font=get_bold_font(font_size))


def draw_sub_title(image_draw: ImageDraw, text: str, draw_right: bool = False):
    font_size = 60
    static_y = 880
    if not draw_right:
        return image_draw.text((70, static_y), text, font=get_bold_font(font_size))
    calculated_text_size = int(image_draw.textlength(text, font=get_bold_font(font_size)))
    return image_draw.text((1850 - calculated_text_size, static_y), text, font=get_bold_font(font_size))


def draw_description(image_draw: ImageDraw, text: str, draw_right: bool = False):
    font_size = 48
    static_y = 950
    if not draw_right:
        return image_draw.text((70, static_y), text, font=get_regular_font(font_size))
    calculated_text_size = int(image_draw.textlength(text, font=get_regular_font(font_size)))
    return image_draw.text((1850 - calculated_text_size, static_y), text, font=get_regular_font(font_size))


def generate_image(background_image: UploadFile, title: str, sub_title: str, description: str, draw_right: bool = False,
                   logo_1: UploadFile = None, logo_2: UploadFile = None,
                   logo_3: UploadFile = None) -> BytesIO:
    image = Image.new("RGBA", (1920, 1080))

    paste_background_with_shade(image, background_image.file)

    if logo_1:
        paste_logo_position_1(image, logo_1.file)
    if logo_2:
        paste_logo_position_2(image, logo_2.file)
    if logo_3:
        paste_logo_position_3(image, logo_3.file)

    draw = ImageDraw.Draw(image)

    draw_title(draw, title, draw_right)
    draw_sub_title(draw, sub_title, draw_right)
    draw_description(draw, description, draw_right)
    output = BytesIO()
    image.save(output, "PNG")
    output.seek(0)

    return output
