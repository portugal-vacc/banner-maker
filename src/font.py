from PIL import ImageFont


def get_regular_font(size: int):
    return ImageFont.truetype("./fonts/myriad_pro_regular.otf", size)


def get_bold_font(size: int):
    return ImageFont.truetype("./fonts/myriad_pro_bold.otf", size)

