from datetime import datetime
from typing import Optional

from fastapi import APIRouter, Depends, UploadFile
from fastapi_simple_security import api_key_security
from starlette.responses import StreamingResponse

from src.cpt import generate_cpt_banner
from src.normal_event import generate_image

api = APIRouter(prefix="/api")


@api.post("/generate/event", response_class=StreamingResponse, dependencies=[Depends(api_key_security)])
def post_generate_event_banner(background_image: UploadFile, title: str,
                               sub_title: str, description: str, draw_right: bool = False,
                               logo_1: Optional[UploadFile] = None, logo_2: Optional[UploadFile] = None,
                               logo_3: Optional[UploadFile] = None):
    return StreamingResponse(content=generate_image(background_image=background_image,
                                                    draw_right=draw_right,
                                                    logo_1=logo_1,
                                                    logo_2=logo_2,
                                                    logo_3=logo_3,
                                                    title=title,
                                                    sub_title=sub_title,
                                                    description=description), media_type="image/png")


@api.post("/generate/cpt", response_class=StreamingResponse, dependencies=[Depends(api_key_security)])
def post_generate_cpt_banner(background_image: UploadFile, rating: str,
                             call_sign: str, start_date_time: datetime, end_date_time: datetime, candidate: str,
                             draw_right: bool = False,
                             logo_1: Optional[UploadFile] = None, logo_2: Optional[UploadFile] = None,
                             logo_3: Optional[UploadFile] = None):
    return StreamingResponse(content=generate_cpt_banner(background_image=background_image,
                                                         draw_right=draw_right,
                                                         logo_1=logo_1,
                                                         logo_2=logo_2,
                                                         logo_3=logo_3,
                                                         rating=rating,
                                                         call_sign=call_sign,
                                                         candidate=candidate,
                                                         start_date_time=start_date_time,
                                                         end_date_time=end_date_time), media_type="image/png")
