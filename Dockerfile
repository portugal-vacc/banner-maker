# syntax=docker/dockerfile:1
FROM python:3.10-slim

WORKDIR /code

RUN pip install --upgrade pip


ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .