# Banner Maker

### Problem
The need to open a image editor to edit a template of a CPT

### Solution
Webpage that creates the image with the required image and text data

### Configuration
![img.png](img.png)

Description
1. Title
2. Template Selector
3. Start and End Datetime Picker
4. Controller Name
5. Generate Button

### Templates
Are defined on another [repository](https://gitlab.com/portugal-vacc/banners/banner-templates) where we configure templates

### For CPT:
#### Start and End Datetime
- You submit 
  - Start: 23/02/2023 20:30 End: 23/02/2023 22:30
- It will output 23rd February | 2030-2230Z

#### Controller Name
For the `Candidate | Placeholder`

### Output
It will download on browser and will be named as {Controller Name}.jpg


## Manutenção
No primeiro deployment, faz favor de ir aos docs da API, fazer login com o que estiver na envvar "FASTAPI_SIMPLE_SECURITY_SECRET" e depois ir aos endpoints de auth para gerar um token.
Após ter token novo, meter na envvar "APIKEY"
